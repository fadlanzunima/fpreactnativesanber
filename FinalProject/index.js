import React from 'react';
import 'react-native-gesture-handler';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import {
    StyleSheet,
    Text,
    View,
    Button,
    ScrollView,
    TextInput,
    TouchableOpacity,
    Image,
    SafeAreaView
} from 'react-native';
import Splash from "./Splash";


// Screen Home
function LoginScreen({navigation}) {
    return (
        <SafeAreaView style={styles.containerSafe}>
            <ScrollView>
                <View style={styles.containerSplash}>
                    <Text style={styles.titleText}>Welcome Back</Text>
                    <Text style={{color: '#4D4D4D', fontSize: 10}}>Sign in to continue</Text>
                    <View style={styles.wrapperForm}>
                        <View style={{marginTop: 20, width: '100%'}}>
                            <Text style={{color: '#4D4D4D', fontSize: 12}}>Email</Text>
                            <TextInput
                                style={{
                                    borderBottomColor: '#E6EAEE',
                                    borderBottomWidth: 2,
                                    color: '#4C475A',
                                    fontSize: 15
                                }}
                                value={'email@email.com'}/>
                        </View>
                        <View style={{marginTop: 20, marginBottom: 10, width: '100%'}}>
                            <Text style={{color: '#4D4D4D', fontSize: 12}}>Password</Text>
                            <TextInput
                                style={{
                                    borderBottomColor: '#E6EAEE',
                                    borderBottomWidth: 2,
                                    color: '#4C475A',
                                    fontSize: 15
                                }}
                                value={'******'}/>
                        </View>
                        <Text style={{textAlign: 'right', width: '100%'}}>Forgot Password?</Text>

                        <TouchableOpacity style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            width: 150,
                            height: 50,
                            backgroundColor: '#F77866',
                            borderRadius: 6,
                            marginTop: 40
                        }} onPress={() => navigation.navigate('Home')}>
                            <Text style={{color: '#fff', fontSize: 20}}>Sign In</Text>
                        </TouchableOpacity>

                        {/*<Text style={{color: '#4C475A', fontSize: 15, marginTop: 15, marginBottom: 15}}>-OR-</Text>*/}

                        {/*<View style={{flexDirection: 'row', justifyContent: 'space-between'}}>*/}
                        {/*    <TouchableOpacity style={{*/}
                        {/*        flexDirection: 'row',*/}
                        {/*        justifyContent: 'center',*/}
                        {/*        alignItems: 'center',*/}
                        {/*        width: 150,*/}
                        {/*        height: 50,*/}
                        {/*        backgroundColor: '#fff',*/}
                        {/*        borderColor: '#E6EAEE',*/}
                        {/*        borderWidth: 1,*/}
                        {/*        borderRadius: 6,*/}
                        {/*        marginTop: 40,*/}
                        {/*        marginRight: 10,*/}
                        {/*    }}>*/}
                        {/*        <Image source={require('./images/fb.png')} style={{width: 20, resizeMode: 'contain', marginRight: 10}}/>*/}
                        {/*        <Text style={{color: '#4D4D4D', fontSize: 15}}>Facebook</Text>*/}
                        {/*    </TouchableOpacity>*/}
                        {/*    <TouchableOpacity style={{*/}
                        {/*        flexDirection: 'row',*/}
                        {/*        justifyContent: 'center',*/}
                        {/*        alignItems: 'center',*/}
                        {/*        width: 150,*/}
                        {/*        height: 50,*/}
                        {/*        backgroundColor: '#fff',*/}
                        {/*        borderColor: '#E6EAEE',*/}
                        {/*        borderWidth: 1,*/}
                        {/*        borderRadius: 6,*/}
                        {/*        marginTop: 40,*/}
                        {/*        marginRight: 10,*/}
                        {/*    }}>*/}
                        {/*        <Image source={require('./images/google.png')} style={{width: 20, resizeMode: 'contain', marginRight: 10}}/>*/}
                        {/*        <Text style={{color: '#4D4D4D', fontSize: 15}}>Facebook</Text>*/}
                        {/*    </TouchableOpacity>*/}
                        {/*</View>*/}
                    </View>
                </View>
            </ScrollView>
        </SafeAreaView>
        // <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        //     <Text>Home Screen</Text>
        //     <Button
        //         title="Go to Details"
        //         onPress={() => navigation.navigate('Details')}
        //     />
        //
        //     <Button
        //         color="red"
        //         title="Go to Profile"
        //         onPress={() => navigation.navigate('Profiles')}
        //     />
        //
        // </View>
    );
}


// Screen Detail
function HomeScreen({route, navigation}) {
    return (
        <SafeAreaView style={{height: '100%'}}>
            <ScrollView>
                <View style={{padding: 10}}>
                    <View style={{flexDirection: 'row'}}>
                        <TouchableOpacity style={styles.searchBox}>
                            <Image style={{resizeMode: 'contain', width: 20}}
                                   source={require('./images/search-logo.png')}/>
                            <TextInput style={{width: 150}} placeholder={'Search Product'}/>
                            <Image style={{resizeMode: 'contain', width: 20}}
                                   source={require('./images/photo-camera.png')}/>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={{width: '10%', justifyContent: 'center', alignItems: 'center', marginLeft: 5}}
                            onPress={() => navigation.navigate('Profiles')}>
                            <Image style={{resizeMode: 'contain', width: '100%'}}
                                   source={require('./images/profilr.png')}/>
                        </TouchableOpacity>
                    </View>
                    <View style={{marginTop: 10}}>
                        <Image style={{resizeMode: 'contain', width: '100%'}}
                               source={require('./images/Slider.png')}/>
                    </View>
                    <View style={{flexDirection: 'row', width: '100%', justifyContent: 'space-between'}}>
                        <View style={{flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
                            <Image style={{resizeMode: 'contain', width: 70}}
                                   source={require('./images/Man.png')}/>
                            <Text>Man</Text>
                        </View>
                        <View style={{flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
                            <Image style={{resizeMode: 'contain', width: 70}}
                                   source={require('./images/women.png')}/>
                            <Text>Women</Text>
                        </View>
                        <View style={{flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
                            <Image style={{resizeMode: 'contain', width: 70}}
                                   source={require('./images/kids.png')}/>
                            <Text>Kids</Text>
                        </View>
                        <View style={{
                            paddingTop: 7,
                            position: 'relative',
                            width: '20%',
                            flexDirection: 'column',
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}>
                            <View style={{marginBottom: 12}}>

                                <Image style={{resizeMode: 'contain', width: 70, zIndex: 0}}
                                       source={require('./images/home-bg.png')}/>
                                <Image style={{
                                    resizeMode: 'contain',
                                    width: 70,
                                    position: 'absolute',
                                    top: 10,
                                    left: 0,
                                    zIndex: 1
                                }}
                                       source={require('./images/home-logo.png')}/>
                            </View>
                            <Text>Home</Text>
                        </View>

                        <View style={{flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
                            <Image style={{resizeMode: 'contain', width: 70}}
                                   source={require('./images/more.png')}/>
                            <Text>More</Text>
                        </View>
                    </View>
                    <View>
                        <Text style={{padding: 10}}>TEST</Text>
                        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                            <Image style={{width: 100, height: 150}} source={require('./images/card-litte.png')}/>
                            <Image style={{width: 100, height: 150}} source={require('./images/card-litte.png')}/>
                            <Image style={{width: 100, height: 150}} source={require('./images/card-litte.png')}/>
                        </View>
                    </View>
                </View>
            </ScrollView>
        </SafeAreaView>
        // <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        //     <Text>Details Screen</Text>
        //     <Button
        //         title="Go to Details... again"
        //         onPress={() => navigation.push('Details')}
        //     />
        //     <Button title="Go to Home" onPress={() => navigation.navigate('Home')}/>
        //     <Button title="Go back" onPress={() => navigation.goBack()}/>
        //     <Button
        //         title="Go back to first screen in stack"
        //         onPress={() => navigation.popToTop()}
        //     />
        // </View>
    );
}

// Screen Profile
function ProfilesScreen({route, navigation}) {
    return (
        <View>
            <View style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: '#6967D6',
                height: '60%',
                width: '100%'
            }}>
                <Image source={require('./images/my-profile.png')}/>
                <Text style={{paddingTop: 15, fontSize: 25, color: 'white'}}>Fadlan Zunima</Text>
            </View>
            <View style={{marginTop: 15, padding: 15}}>
                <Text style={{color: '#24B9E8', fontSize: 25}}>Social</Text>
                <Image style={{marginTop: 20}} source={require('./images/fblogo.png')}/>
                <Image style={{marginTop: 20}} source={require('./images/twitter.png')}/>
            </View>
        </View>
        // <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        //     <Text>Profile Screen</Text>
        //     <Button
        //         color="black"
        //         title="Go to Home"
        //         onPress={() => navigation.navigate('Home')}
        //     />
        //     <Button title="Go back" onPress={() => navigation.goBack()}/>
        //     <Button
        //         color="green"
        //         title="Go back to first screen in stack"
        //         onPress={() => navigation.popToTop()}
        //     />
        // </View>
    );
}

// Stack berguna untuk routing aplikasi
const Stack = createStackNavigator();

function Index() {
    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName="Splash">
                <Stack.Screen name="Splash" component={Splash} options={{headerShown: false}} />
                <Stack.Screen name="Login" component={LoginScreen}/>
                <Stack.Screen name="Home" component={HomeScreen}/>
                <Stack.Screen name="Profiles" component={ProfilesScreen}/>
            </Stack.Navigator>
        </NavigationContainer>
    )
}

export default Index;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    containerSplash: {
        padding: 10,
    },
    titleText: {
        fontSize: 33,
        color: '#0C0423',
        fontWeight: 'bold'
    },
    wrapperForm: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 25,
        width: '100%',
        height: 325,
        padding: 15,
        backgroundColor: '#FFFFFF',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        elevation: 4,
        borderRadius: 10
    },
    searchBox: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderColor: 'black',
        width: '90%',
        height: 50,
        borderRadius: 10,
        borderWidth: 1,
        padding: 10
    }
});
